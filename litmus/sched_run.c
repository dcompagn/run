/*
 * litmus/sched_run.c
 *
 * Implementation of the RUN scheduling algorithm.
 *
 */
/* 17/12/2013 - 1)Corretto comportamento esecuzione a ZL*/
/*				2)Definito errore di arrotondamento*/
/* 21/06/2013 - 1)Gestione tardy jobs on completion*/
/* 20/06/2013 - 1)Corretta terminazione anche su albero degenere*/ 
/* 19/06/2013 - 1)Aggiunto entry->resched per reschedule server*/
/* 18/06/2013 - 1)Problema overrun, aggiunto trace on job_completion e */
/* 		flag ->completed=0 in requeue come psn-edf (risolve?)*/
/* 31/05/2013 - 1)Modifica del release con barriera (release_ts)*/
/* 		Uso della coda globale prima del release del sistema*/
/* 31/05/2013 - 1)Pulizia struture dati plugin, tiny stack globale*/
/* ---------------------------------------------------------------------------*/
/* 30/05/2013 - 1)Collassamento dell'albero per rami con branch 1 */
/*		(es. S5 paper) (correzione resched_server)*/
/*		2)Aggiunta istante di release ai nodi per avere il dimensionamento*/
/*		corretto del budget, non basato su litmus_clock() (Full Utilization)*/
/*		(corrette update_deadline_on_tree, on_server_budget, ecc.)*/
/*		3)Corretta terminazione nell'"_update_r_node" (prime righe)*/
/* ---------------------------------------------------------------------------*/

#include <asm/uaccess.h>
#include <linux/spinlock.h>
#include <linux/percpu.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/hrtimer.h>

#include <litmus/litmus.h>
#include <litmus/jobs.h>
#include <litmus/sched_plugin.h>
#include <litmus/edf_common.h>
#include <litmus/sched_trace.h>
#include <litmus/trace.h>

#include <litmus/preempt.h>
#include <litmus/budget.h>
#include <litmus/bheap.h>

#include <litmus/sched_run.h>

/*Uncomment this if you want to see all scheduling decisions in the TRACE() log.*/
/*#define WANT_BASE_SCHED_EVENTS
#define WANT_ALL_SCHED_EVENTS
#define WANT_TIMERS_SCHED_EVENTS
#define WANT_SERVERS_SCHED_EVENTS
#define WANT_ONLY_SCHED_EVENTS*/

/*Uncomment this if you want to force experiment termination*/
#define TERMINATION_CODE
#define TERMINATION_TIME 3500LL // termination delay in ms
/* RUN SERVER. Maintains the information of the runtime status of a leaf
 * server for the reduction tree.
 *
 * id          : unique identifier
 * cpu         : last cpu that scheduled the server
 * running     : flag to identify if the server is currently scheduled
 * released    : flag to identify a new release inside the server
 *
 * highest_prio: the task that the server executes when running
 * ready_queue : edf ready queue for the ready tasks
 * rn          : pointer to the node structure of the reduction tree
 */
typedef struct run_server {
	
	int 					id;
	int						cpu;
	int						running;
	int						released;
	
	struct task_struct* 	highest_prio;
	struct bheap	 		ready_queue;	
	struct r_node*			rn;

	/* -- LIMA */
	int 					task_n;
	struct task_struct*		task_ref[MAX_TASK_FOR_SERVER];
	/* -- LIMA */

} run_server_t;

/* CPU ENTRY. Maintains the runtime status of a cpu
 *
 * cpu         : identifier for a specific cpu
 * resched     : flag to notify the cpu must perform a rechedule. Probably
 *               because a different server must be scheduled.
 * sched       : task currently being scheduled on the cpu
 * s_sched     : server currently being scheduled on the cpu
 */
typedef struct  {
	
	int 				cpu;
	int 				resched;
	struct task_struct* sched;
	run_server_t*		s_sched;
		
} cpu_entry_t;

DEFINE_PER_CPU(cpu_entry_t, cpu_entries);

/*
 * RUN DOMAIN. Maintains the data structures necessary to RUN. It is a global
 * structure.
 *
 * n_cpus    : numbers of available cpus
 * n_servers : numbers of leaf servers for the given taskset
 * ts        : support structure to optimize the rescheduling events on the cpu.
 * domain    : an edf domain. It is used to gather releases of multiple tasks in
 *             a single event
 * rtree     : the reduction tree for the given taskset
 * servers   : list of all the leaf servers
 */
typedef struct {
	
	int					n_cpus;
	int					n_servers;
	
	struct tiny_stack 	ts;
	
	rt_domain_t 		domain;
	r_tree_t 			rtree;
	run_server_t 		*servers[MAX_SERVER];
	
} run_domain_t;

run_domain_t			run;

lt_t 					global_time;

/* Global lock needed perform scheduling operations. A global lock is
 * necessary since multiple events can be fired from different cpus and the
 * RUN algorithm has global data structures (reduction tree)
 */
raw_spinlock_t			slock;

/* ------- Termination code ------- */
#ifdef TERMINATION_CODE
int 						tasks_added;
int 						set_exit_timer;
int 						exit_mode;
struct hrtimer 				exit_timer;
#endif
/* ------- Termination code ------- */

/* Support structure needed to build the reduction tree
 */
r_node_t					*curr_node;

/* Useful define */

#define ONE_MS 		1000000LL

#define local_entry			(&__get_cpu_var(cpu_entries))
#define	remote_entry(cpu) 	(&per_cpu(cpu_entries, cpu))

#define get_s_id(task)	task->rt_param.task_params.run_server_id
#define get_s(task) 	task->rt_param.server

#define s_from_dom(dom) \
	(container_of((dom), run_server_t, domain))

#define system_started \
	(lt_after(global_time, INIT_HORIZON) && \
		lt_after_eq(litmus_clock(), global_time))

#define run_printk(fmt, arg...) printk(KERN_INFO fmt, ##arg)

// Initialize an empty server
static void init_server(run_server_t *server, int id, r_node_t *rn) 
{
	server->id = id;
	server->cpu = NO_CPU;
	server->running = 0;
	server->released = 0;
	server->highest_prio = NULL;	
	bheap_init(&server->ready_queue);
	server->rn = rn;

	/* -- LIMA */
	server->task_n = 0;
	/* -- LIMA */

}

// Insert information on a specific node of the reduction tree
static void set_node_data(r_node_t *node, r_node_t* l_c, r_node_t* r_s, 
				r_node_t* parent, int id, lt_t rate_a, 
				lt_t rate_b, int level) 
{
	node->l_c = l_c;
	node->r_s = r_s;
	node->parent = parent;
	node->id = id;
	node->rate[0] = rate_a;
	node->rate[1] = rate_b;
	node->level = level;	
}

// SYSCALL Add a node in the reduction tree. Used while building the reduction tree
asmlinkage long sys_run_add_node(int __user *__id, lt_t __user *__rate_a, 
				lt_t __user *__rate_b, int __user *__level)
{
	long ret;
	int id; 
	lt_t rate_a;
	lt_t rate_b;
	int level;
	
	r_node_t *node;
	run_server_t *server;
	
	ret = copy_from_user(&id, __id, sizeof(id));
	if (ret) return ret;
	ret = copy_from_user(&rate_a, __rate_a, sizeof(rate_a));
	if (ret) return ret;
	ret = copy_from_user(&rate_b, __rate_b, sizeof(rate_b));
	if (ret) return ret;
	ret = copy_from_user(&level, __level, sizeof(level));
	if (ret) return ret;
	
	if (!curr_node) {
		node = kmalloc(sizeof(r_node_t), GFP_ATOMIC);
		t_st_push(&run.ts, node);
		curr_node = node;
		run.rtree.root = node;
		run.rtree.level = level;
		INIT_R_NODE(node);
		set_node_data(node, NULL, NULL, NULL, id, rate_a, rate_b, level);			
		node->color = 0;		
	} else {
		if (curr_node && curr_node->color == 0) {
			if (id != -1) {			
				node = kmalloc(sizeof(r_node_t), GFP_ATOMIC);
				t_st_push(&run.ts, node);
				INIT_R_NODE(node);
				set_node_data(node, NULL, NULL, curr_node, id, rate_a, rate_b, level);
				node->color = 0;
				curr_node->color = 1;
				curr_node->l_c = node;
				curr_node = node;
			} else {
				curr_node->color = 1;
				if (curr_node->l_c == NULL) {
					server = kmalloc(sizeof(run_server_t), GFP_ATOMIC);
					init_server(server, curr_node->id, curr_node);
					TRACE("Server %d added (node %d)\n", server->id, curr_node->id);
					BUG_ON(run.servers[server->id]);						
					run.servers[server->id] = server;
					run.n_servers += 1;
				}			
			}
			return ret;
		}
		if (curr_node && curr_node->color == 1) {
			if (id != -1) {
				node = kmalloc(sizeof(r_node_t), GFP_ATOMIC);
				t_st_push(&run.ts, node);			
				INIT_R_NODE(node);
				set_node_data(node, NULL, NULL, curr_node->parent, id, rate_a, rate_b, level);
				node->color = 0;
				curr_node->color = 2;
				curr_node->r_s = node;
				curr_node = node;
			} else {
				curr_node->color = 2;
				curr_node = (r_node_t *)t_st_pop(&run.ts);
				while(curr_node && curr_node->color == 2)
					curr_node = (r_node_t *)t_st_pop(&run.ts);
			}
		}
	}
	return ret;
	
}

/* Assigns the ready tasks to the rightful owner. Function used when a job of a
 * task is released. Since all release events of the tasks are managed inside
 * the run_domain_t "run->domain" (which is a global EDF domain), it is
 * necessary to insert the released tasks inside the servers that will schedule
 * them. The server is the one decided during the offline phase that produce
 * the reduction tree.
 */
static void run_add_ready(struct task_struct *t) {
	run_server_t *s;
	
	BUG_ON(bheap_node_in_heap(tsk_rt(t)->heap_node));
	s = get_s(t);
	bheap_insert(edf_ready_order, &s->ready_queue, tsk_rt(t)->heap_node);
	
	#ifdef WANT_ALL_SCHED_EVENTS
	TRACE_TASK(t, "run_add_ready: released on server %d\n", s->id);
	#endif
	
}

/* Extracts the highest priority task from the ready queue with respect to
 * an EDF policy.
 */
static struct task_struct* run_take_ready(run_server_t *s)
{
	struct bheap_node* hn = bheap_take(edf_ready_order, &s->ready_queue);
	return (hn) ? bheap2task(hn) : NULL;
}

/* Determines the highest priority task inside the ready queue with respect to
 * an EDF policy.
 */
static struct task_struct* run_peek_ready(run_server_t *s)
{
	struct bheap_node* hn = bheap_peek(edf_ready_order, &s->ready_queue);
	return (hn) ? bheap2task(hn) : NULL;
}

/* Requeues the task.
 * If the task has a running job active (that has not completed execution), the
 * task is placed in the ready queue of the right server. The server is the
 * one decided during the offline phase that produce the reduction tree.
 * If the task has a completed job, the task is placed in the release queue
 * of the RUN edf_domain.
 *
 * IMPORTANT: if a job has completed execution, this procedure must be called
 *            after "job_completion". It is important that
 *            "prepare_for_next_period" is already performed on the task.
 */
static noinline void requeue(struct task_struct* task)
{	
	
	BUG_ON(!task);
	/* sanity check before insertion */
	BUG_ON(is_queued(task));
	
	tsk_rt(task)->completed = 0;
	
	// if the task has an active not-completed job, requeue it in the server.
	if (is_released(task, litmus_clock())) {
		run_add_ready(task);
	}
	// if the task has a completed job, requeue it in the run edf_domain.
	// We assume that a completed job has already called (possibly indireclty)
	// the "prepare_for_next_period" procedure, this way the completed job is not
	// considered, and the next prepared job will fail the test "is_released".
	else {
		add_release(&run.domain, task);
	}

}

static noinline void init_requeue(struct task_struct* task)
{
	BUG_ON(!task);
	/* sanity check before insertion */
	BUG_ON(is_queued(task));
	
	tsk_rt(task)->completed = 0;
	
	if (is_released(task, litmus_clock()))
		__add_ready(&run.domain, task);
	else {
		// it has got to wait
		add_release(&run.domain, task);
	}
}

/* Propagates the deadline in the subtree growing from node n up to the root.
 * This is necessary to compute new budgets and to correctly schedule the
 * child nodes of a pack server of the reduction tree.
 */
/*static void _update_deadline_on_tree(r_node_t *n, lt_t now, lt_t dl) {
	
	int			updated;
	r_node_t 	*tmp;
	
	tmp = n;
	
	#ifdef WANT_BASE_SCHED_EVENTS
	if (lt_after_eq(now, dl)) {
		TRACE("WARNING: NOW is after DL (node %d)\n", n->id);
	}
	#endif	
	while (tmp) {		
		
		#ifdef WANT_ALL_SCHED_EVENTS
		TRACE("node %d,now: %llu, dl: %llu, tmp->sched->e_dl: %llu, tmp->rel: %llu, tmp->e_dl: %llu\n", tmp->id, now, dl, (tmp->sched ? tmp->sched->e_dl : INIT_HORIZON), tmp->rel, tmp->e_dl);
		#endif

		updated = 0;
		if (lt_after_eq(now, tmp->e_dl)) {
			
			// This update the release time of a node (previous deadline)
			if (unlikely(tmp->e_dl == INIT_HORIZON)) {
				tmp->rel = now;
			} else {	
				tmp->rel = tmp->e_dl;
			}

			// Second condition guarantees [now|tmp->sched->e_dl|dl] order
			if (tmp->sched && lt_after(tmp->sched->e_dl, now) 
					&& lt_after(dl, tmp->sched->e_dl)) {
				tmp->e_dl = tmp->sched->e_dl;
			} else { 
				tmp->e_dl = dl;
			}
			updated = 1;
		} else {
			#ifdef WANT_ALL_SCHED_EVENTS
			if(now != tmp->rel)
				TRACE("now %llu, is not equal to tmp->rel %llu\n", now, tmp->rel);
			#endif

			if (lt_after(tmp->e_dl, dl) && lt_after(dl, tmp->rel)) {
			//if ((now == tmp->rel) && lt_after(tmp->e_dl, dl)) {
				tmp->e_dl = dl;
				updated = 1;
			}
		}
		
		if (updated) {
			BUG_ON(tmp->rate[1] == 0);
			BUG_ON(lt_after(tmp->rel, tmp->e_dl));
			tmp->budget = (((tmp->e_dl - tmp->rel) * tmp->rate[0]) 
								/ tmp->rate[1]);
			#ifdef WANT_ALL_SCHED_EVENTS
			TRACE("node: %d, budget updated: %llu\n", tmp->id, tmp->budget);
			#endif
		}	
				
		tmp = tmp->parent;
	}
}*/

/* -- LIMA */

static void update_deadline_on_tree(r_node_t *n, lt_t now, lt_t dl) {
	
	int			updated;
	r_node_t 	*tmp;
	lt_t 		e_dl;

	tmp = n;
	
	#ifdef WANT_BASE_SCHED_EVENTS
	if (lt_after_eq(now, dl)) {
		TRACE("WARNING: NOW is after DL (node %d)\n", n->id);
	}
	#endif	
	while (tmp) {		
		
		//#ifdef WANT_ALL_SCHED_EVENTS
		//TRACE("node %d,now: %llu, dl: %llu, tmp->sched->e_dl: %llu, tmp->rel: %llu, tmp->e_dl: %llu\n", tmp->id, now, dl, (tmp->sched ? tmp->sched->e_dl : INIT_HORIZON), tmp->rel, tmp->e_dl);
		//#endif

		updated = 0;
		
		e_dl = get_earliest_node_deadline(tmp, now);

		if (lt_after_eq(now, tmp->e_dl)) {
			
			/* This update the release time of a node (previous deadline) */
			if (unlikely(tmp->e_dl == INIT_HORIZON)) {
				tmp->rel = now;
			} else {	
				tmp->rel = tmp->e_dl;
			}

			if (e_dl == INIT_HORIZON) {
				if (dl == INIT_HORIZON) {
					#ifdef WANT_ALL_SCHED_EVENTS
						TRACE("WARNING: no earliest deadline\n");
					#endif
					tmp->e_dl = now;
				} else {
					tmp->e_dl = dl;
				}
			} else {
				tmp->e_dl = e_dl;
			}
			updated = 1;
		} else {

			if ((tmp->rel == now) && lt_after(tmp->e_dl, e_dl)) {
				tmp->e_dl = e_dl;
				updated = 1;
			}

			#ifdef WANT_ALL_SCHED_EVENTS
			if (tmp->rel != now)
				TRACE("WARNING: now %llu before tmp->edl %llu\n", now, tmp->rel);
			#endif	
		}
		
		if (updated) {
			BUG_ON(tmp->rate[1] == 0);

			if (tmp->e_dl == INIT_HORIZON)
				tmp->budget = 0;
			else
				tmp->budget = (((tmp->e_dl - tmp->rel) * tmp->rate[0]) 
								/ tmp->rate[1]);

			#ifdef WANT_ALL_SCHED_EVENTS
			TRACE("node: %d, updated: rel %llu e_dl %llu budget %llu\n", tmp->id, tmp->rel, tmp->e_dl, tmp->budget);
			#endif
		}	
		
		tmp = tmp->parent;
	}
}

/* -- LIMA */

/* Determines whether a preemption should occur inside a server.
 */
static int preemption_needed(run_server_t *s) {

	struct task_struct *a, *b;
	
	a = s->highest_prio;
	b = run_peek_ready(s);

	return edf_higher_prio(b, a);
}


/* Updates the timers of the reduction tree. These timers are used to simulate
 * the execution of non-leaf-nodes. When a timer is fired, it means that a
 * budget exaustion event happend.
 * IMPORTANT : to be called with IRQs off
 */
void node_update_timer(r_node_t* n, lt_t now)
{	
	int ret;
	lt_t when_to_fire;
	
	if (!n) return;
	
	if ((n->circled == 0) && (n->sched) && (n->sched->circled)) { 
		// If we are here, the node is active
		when_to_fire = n->sched->start_time + n->sched->budget;
		#ifdef WANT_TIMERS_SCHED_EVENTS
		TRACE("start %llu, budget %llu, w_t_f %llu\n", n->sched->start_time, n->sched->budget, when_to_fire);
		#endif
		if (lt_after_eq(when_to_fire, n->sched->e_dl) ||
			((n->sched->e_dl - when_to_fire) < ROUND_ERROR) ) {			
			n->sched->is_ZL = 1;	
			#ifdef WANT_TIMERS_SCHED_EVENTS
			TRACE("node %d timer not armed, child %d at ZL execution\n",
					n->id, n->sched->id);
			#endif
		} else {
			//if((lt_after_eq(when_to_fire, n->e_dl) && (when_to_fire - n->e_dl < ROUND_ERROR)) || 
			//	(lt_after(n->e_dl, when_to_fire) && (n->e_dl - when_to_fire < ROUND_ERROR))) {
			//	TRACE("node %d timer not armed for child %d, wtf near release\n", n->id, n->sched->id);	
			//} else {
				#ifdef WANT_TIMERS_SCHED_EVENTS
				TRACE("arming node timer, budget %llu w_t_f %llu\n",
						 n->sched->budget, when_to_fire);
				#endif
					
				__hrtimer_start_range_ns(&n->timer,
						 ns_to_ktime(when_to_fire),
						 0, HRTIMER_MODE_ABS_PINNED, 0);
						 
				n->armed = 1;
			//} 
		}
					
	} else if (n->armed) {
		// If the node is inactive and the timer is armed, we have to shut it down
		
		#ifdef WANT_TIMERS_SCHED_EVENTS
		TRACE("cancelling enforcement timer\n");
		#endif
		
		//if (n->sched) {		
			ret = hrtimer_try_to_cancel(&n->timer);
			
			#ifdef WANT_BASE_SCHED_EVENTS
			if (ret == 0 || ret == -1)			
				TRACE("WARNING ret == %d\n", ret);
			#endif
		//}
		
		n->armed = 0;	
	}
}

static void _update_r_node(r_node_t *n, lt_t now)
{
	r_node_t*	tmp;	
	int			active, exists, preempt, update, ZL;
	lt_t 		w_t_f;
	
	BUG_ON(!n);
	
	// Check to determine if the nodes must be terminated. To be able to shut
	// down the whole scheduling algorithm, it is necessary that every task has
	// completed execution. Since a task can execute only if its server has
	// spared budget, and since the budget is replenished at the release of some
	// tasks, if these tasks are already terminated, then no budget is updated
	// and some tasks can never have any chance to execute to terminate. This
	// check prevents this situation.

	if (unlikely(lt_after_eq(now, n->e_dl) && (n->e_dl != INIT_HORIZON))) {
		#ifdef WANT_ALL_SCHED_EVENTS
		TRACE("WARNING: now %llu after node %d e_dl %llu\n", now, n->id, n->e_dl);
		#endif
		//140120 - Is this necessary?
		return;
	}
	
	if ((n->level == 0) || (!n->l_c)) return;
	
	#ifdef WANT_ALL_SCHED_EVENTS
	TRACE("Updating node %d\n", n->id);
	#endif
	
	active = (n->circled == 0);
	
	//Se branching 1 ritorna NULL sempre e aggiorni a mano
	tmp = get_earliest_node(n);
	
	exists = (tmp != NULL);
	preempt = (exists && (tmp != n->sched));
	update = 0;
		
	// If a node is active and its child needs to be preempted or if the node
	// is inactive while one of its child is active, we must stop the execution
	// of the child.
	if ((active && preempt) || (!active)) {
		if (n->sched && n->sched->circled) {			
			sched_node_stop_exec(n->sched, now);
			update = 1;
			#ifdef WANT_ALL_SCHED_EVENTS
			TRACE("Node %d, stopping child %d\n",
							n->id, n->sched->id);
			#endif
		}		
	}
	
	// If the node is active but there is a preemption among its children,
	// we must start the execution of the new child.
	if (active && preempt) {
		n->sched = tmp;
		sched_node_start_exec(n->sched, now);
		update = 1;
		#ifdef WANT_ALL_SCHED_EVENTS
		TRACE("Node %d, EARLIEST child %d deadline %llu\n",
							n->id, n->sched->id, n->sched->e_dl);
		#endif
	}
	
	// If the node is active and the scheduled child is not being preempted,
	// we assure that it executes.
	if (active && exists && !preempt) {		
		if (!n->sched->circled) { // non stava eseguendo
			BUG_ON(n->sched->budget <= 0);
			sched_node_start_exec(n->sched, now);
			update = 1;
			#ifdef WANT_ALL_SCHED_EVENTS
			TRACE("Node %d, restarting child %d\n",
							n->id, n->sched->id);
			#endif
		} else {
			w_t_f = n->sched->start_time + n->sched->budget;
			ZL = (lt_after_eq(w_t_f, n->sched->e_dl) ||
					((n->sched->e_dl - w_t_f) < ROUND_ERROR));
			if (!ZL && n->sched->is_ZL) {
				#ifdef WANT_ALL_SCHED_EVENTS
				TRACE("!ZL but was in ZL\n");
				#endif
				n->sched->is_ZL = 0;
				sched_node_start_exec(n->sched, now);
				update = 1;
			}
		}
	}
	
	// If the node is active but no child can be scheduled, we assure that
	// the last scheduled child (if any) stops executing.
	if (active && !exists) {
		
		if (n->sched) {
			BUG_ON(n->sched->budget != 0);

			n->sched->is_ZL = 0;
			//sched_node_stop_exec(n->sched, now);
			uncircle(n->sched);
			
			n->sched = NULL; // = tmp
			update = 1;

			#ifdef WANT_BASE_SCHED_EVENTS
			TRACE("WARNING node %d, stopping sched\n",
							n->id);
			#endif
		}
	}
	
	// If some changes happened, we update the timer of the node. The timer
	// simulates the execution or non-execution of the node's budget.
	if (update) {		
		node_update_timer(n, now);		
	}
}



/* This procedure elaborates the output of the RUN scheduling decision and
 * apply it. Once the reduction tree is updated, this procedure is concerned
 * to understand how to dispatch the servers to the available processors. It
 * is also its duty to free the processors of servers that should not execute
 * and to notify the processors to reschedule their work, since it can be
 * changed.
 *
 * IMPORTANT: must be called with slock
 */
static void run_resched_servers(lt_t now) {
	
	int 			s_id, cpu;
	run_server_t 	*s;
	cpu_entry_t		*entry;		
	int 			exists, active, release_cpu, circled;
	
	t_st_init(&run.ts);

	for(cpu = 0; cpu < run.n_cpus; cpu ++) {	
		entry = remote_entry(cpu);
		s = entry->s_sched;
		exists = (s != NULL);

		release_cpu = exists && (
				(!(s->rn->level % 2) && (s->rn->circled)) ||
				((s->rn->level % 2) && !(s->rn->circled))
			      );
		// 140123 - active make no sense if budget exh event happen before release even though it was before
		active = exists && /*lt_after_eq(s->rn->e_dl, now) &&*/ ((s->highest_prio) || (!bheap_empty(&s->ready_queue)));
		//old - active = exists && (lt_after_eq(s->rn->e_dl, now) || ((s->highest_prio) || (!bheap_empty(&s->ready_queue)));
		
		// If the processor was not running anything, then consider it as a
		// processor that can be used to run something
		if (!exists) {
			t_st_push(&run.ts, entry);
			#ifdef WANT_ALL_SCHED_EVENTS			
			TRACE("Server NULL is releasing the cpu #%d\n", cpu);
			#endif
		}
		
		// If the server that was being executed by the processor must not be
		// running anymore, remember the cpu as free.
		// We consider the cpu as free (that can receive new work -server- to
		// execute) even if a server is currently being executed, but the server
		// has no more work to do.
		if (release_cpu || (exists && !active)) {
			s->running = 0;
			entry->s_sched = NULL;
			entry->resched = 1;	
			t_st_push(&run.ts, entry);
			#ifdef WANT_ALL_SCHED_EVENTS
			TRACE("Server  %d is releasing the cpu #%d\n", s->id, cpu);
			#endif
		}
	}

	for(s_id = 0; s_id < run.n_servers; s_id++) {	
		s = run.servers[s_id];
		
		circled = (!(s->rn->level % 2) && !(s->rn->circled)) ||
				((s->rn->level % 2) && (s->rn->circled));
		
		//140123 
		active = /*lt_after_eq(s->rn->e_dl, now) &&*/ ((s->highest_prio) || (!bheap_empty(&s->ready_queue)));
		//old - active = lt_after_eq(s->rn->e_dl, now) || ((s->highest_prio) || (!bheap_empty(&s->ready_queue)));

		#ifdef WANT_BASE_SCHED_EVENTS
		if (s->released && !active) 
			TRACE("WARNING: Server  %d\n, release and not active. e_dl: %llu now: %llu\n", s->id, s->rn->e_dl, now);
		#endif
		
		// If a server is decided to not execute, reset possible release events
		// since they will be discarded.
		if (!circled) {
			BUG_ON(s->running);
			s->released = 0;
			#ifdef WANT_SERVERS_SCHED_EVENTS
			TRACE("Server  %d\n", s->id);
			#endif
		} else {
		
			// If a server has work to do, is deemed to be executed and is already
			// running, it means that a processor is being assigned to it.
			// In this case we are only concerned if a job has beeing released for
			// the server, in which case remember to notify the relative processor
			// that it must reschedule its workload.
			if (active && circled && s->running) {
				if (s->released) {
					s->released = 0;
					if (preemption_needed(s)) {
						entry = remote_entry(s->cpu);
						entry->resched = 1;
					}
				}
				#ifdef WANT_SERVERS_SCHED_EVENTS
				TRACE("Server (%d) cpu #%d\n", s->id, s->cpu);
				#endif
			} else {
				
				// Otherwise, if a server has work to perform, is deemed to be executed
				// by RUN but is not already running, we assign the server to a free
				// processor. The free processor is taken from the temporary stack
				// created in the previous step.
				// We also mark the chosen cpu to remember to reschedule its work.
				if (active && circled && !s->running) {
					if (t_st_empty(&run.ts)) {
						#ifdef WANT_SERVERS_SCHED_EVENTS
						TRACE("WARNING: Server (%d) no proc available\n", s->id);
						#endif
					}
					else {
						entry = (cpu_entry_t *)t_st_pop(&run.ts);
						s->cpu = entry->cpu;
						s->running = 1;	
						s->released = 0;			
						entry->s_sched = s;
						entry->resched = 1;
						#ifdef WANT_SERVERS_SCHED_EVENTS
						TRACE("Server (%d) assigned to cpu #%d\n", 
									s->id, entry->cpu);
						#endif
					}
				}
			}
		}
	}
	
	// Notify the processors marked in the previous step to reschedule their
	// work since something changed: a new server is being executed, a new
	// job of the same server must preempt the previous one, no work needs to
	// be performed.
	// We fire the system call to reschedule only the cpu that needs to
	// be rescheduled
	for(cpu = 0; cpu < run.n_cpus; cpu ++) {	
		entry = remote_entry(cpu);
		if (entry->resched) {
			entry->resched = 0;
			litmus_reschedule(cpu);
			#ifdef WANT_ALL_SCHED_EVENTS
			TRACE("litmus_reschedule(%d) at %llu\n", entry->cpu, litmus_clock());
			#endif
		}
	}
}

// Returns the earliest deadline among the tasks of the specified server.
/*static lt_t get_earliest_deadline(run_server_t *s) {
	
	struct task_struct *t;
	
	if (preemption_needed(s)) {
		t = run_peek_ready(s);
		BUG_ON(!t);
		return get_deadline(t);
	} else {
		BUG_ON(!s->highest_prio);
		return get_deadline(s->highest_prio);
	}
}*/

/* -- LIMA */

static lt_t get_job_deadline(struct task_struct * t, lt_t now) {
	if (!t) return now;
	if (is_released(t, now))
		return get_deadline(t);
	else
		return get_release(t);
}

static lt_t get_earliest_deadline(run_server_t *s, lt_t now) {
	
	int 				i;
	lt_t				e_dl, t_dl;
	
	i = 0;
	e_dl = INIT_HORIZON;
	
	while(i < s->task_n) {
		t_dl = get_job_deadline(s->task_ref[i], now);
		if (lt_after(t_dl, now) && (e_dl == INIT_HORIZON))
			e_dl = t_dl;
		if(lt_after(t_dl, now) && lt_after(e_dl, t_dl))
			e_dl = t_dl;
		i += 1;
	}
	
	return e_dl;
}

/* -- LIMA */

/* Manages the releases of jobs. The release of all jobs is managed by the
 * edf_domain inside the RUN global release queue. This procedure is concerned
 * about the assignment of the released jobs to the rightful owner (server).
 * It is also its duty to fire a reschedule event inside each processor
 * since a preemption could be needed.
 *
 * tasks : heap of released tasks. There can be one single event for the
 *         release of multiple tasks.
 */
static void run_release_jobs(rt_domain_t* rt, struct bheap* tasks)
{
	unsigned long 		flags;
	struct task_struct* 	t;
	run_server_t*		s;
	int			s_id;
	lt_t			now, e_dl;
	
	BUG_ON(!system_started);
	
	// Acquire global lock since we need to update the global data structures.
	raw_spin_lock_irqsave(&slock, flags);
	#ifdef WANT_ALL_SCHED_EVENTS
	TRACE("----------------- RELEASE at %llu\n", litmus_clock());
	#endif
	t = bheap2task(bheap_peek(edf_ready_order, tasks));
	BUG_ON(!t);
	// Get the time instant that fired the event. Since all jobs inside the
	// input parameter bheap "tasks" are relative to the same event, obtaining
	// the time from the first element in the list is equivalent to obtaining it
	// from any of the others.
	now = get_release(t);

	// Assign each released job in the server its task belong to.
	// Mark all servers that received a newly released job. This way we can
	// perform selectively additional management operations.
	while(!bheap_empty(tasks)) {		
		t = bheap2task(bheap_take(edf_ready_order, tasks));
		BUG_ON(!t);
		s = get_s(t);				
		run_add_ready(t);	
		s->released = 1;
		#ifdef WANT_ALL_SCHED_EVENTS
		TRACE_TASK(t,"released on server %d\n",s->id);
		#endif
	}
	
	// Propagates up to the root the (possibly) new earliest deadlines. Necessary
	// also to update the budget of the servers, since a release coincides to a
	// budget replenishment event (deadline == period for fixed rate tasks).
	//
	// Consider all the servers that received at least one newly released job.
	// Since a new job is added the earliest deadline could be changed. If the
	// earliest deadline changes, also the deadline of its parent must be updated.
	// If a server has not received any newly released job, then its earliest
	// deadline has already propagated in the tree, and thus no operations must
	// be performed.
	for(s_id = 0; s_id < run.n_servers; s_id++) {
		s = run.servers[s_id];
		#ifdef WANT_ALL_SCHED_EVENTS
		TRACE("server %d released %d\n", s->id, s->released);
		#endif
		if (s->released) {
			e_dl = get_earliest_deadline(s, now);
			if (e_dl == INIT_HORIZON) {
				#ifdef WANT_ALL_SCHED_EVENTS
				TRACE("WARNING: No earliest_deadline found");
				#endif
				update_deadline_on_tree(s->rn, now, now);
			}
			else {
				update_deadline_on_tree(s->rn, now, e_dl);
			}
		} else {
			#ifdef WANT_ALL_SCHED_EVENTS
			if (s->rn->e_dl != INIT_HORIZON && 
					lt_after(now, s->rn->e_dl)) {
				TRACE("WARNING server %d should complete\n");
			}
			#endif
		}
	}
	// Update the status of the reduction tree with the newly received deadlines.
	r_tree_traversal(run.rtree.root, now, &run.ts);
	// Dispatch the servers that the RUN algorithm deemed to be executing.
	run_resched_servers(now);
	
	// Release the global lock.
	#ifdef WANT_ALL_SCHED_EVENTS
	TRACE("----------------- END -----------------\n");
	#endif
	raw_spin_unlock_irqrestore(&slock, flags);
	
}

/* Manage job completion events.
 * Its main duty is to prepare the next job of the task.
 * IMPORTANT: This procedure does not requeue the task in the release queue.
 */
static void job_completion(struct task_struct* t, int forced)
{
	//lt_t now;
	
	BUG_ON(!t);
	
	// Trace job completion event
	sched_trace_task_completion(t, forced);
	#ifdef WANT_BASE_SCHED_EVENTS
	TRACE_TASK(t, "job_completion().\n");
	#endif
	// Set completion tag
	tsk_rt(t)->completed = 1;
	// Compute the next release and deadline.
	prepare_for_next_period(t);
	
//	if (is_released(t, litmus_clock())) {
		//BUG_ON(1);
//		s = get_s(t);	
//		e_dl = get_earliest_deadline(s);
//		if (lt_after(e_dl, get_deadline(t)))
//			e_dl = get_deadline(t);
//		/* When the release should happen */
//		now = get_release(t);
//		update_deadline_on_tree(s->rn, now, e_dl);
//		
//		s->released = 1;
//		
//		r_tree_traversal(run.rtree.root, now, &run.ts);
//		run_resched_servers();

//		#ifdef WANT_BASE_SCHED_EVENTS
//		TRACE_TASK(t, "WARNING: Tardy task %llu\n", (litmus_clock() - now));
//		#endif
//		sched_trace_task_release(t);
//	}
//		
//	/*Here i should call release method to reschedule*/
//	/*see gsn-edf behaviour	*/
}

/******** Timer Management *************************/

/* Manages events arising from the budget of servers.
 * Since replenishment event are handled with the release of tasks, this
 * procedure manages only budget exhaustion events.
 *
 * IMPORTANT: to be called with IRQs off
 */
static enum hrtimer_restart on_server_budget_event(struct hrtimer *timer)
{
	
	r_node_t* n = container_of(timer, r_node_t, timer);
	unsigned long flags;
	lt_t now, ts;

	/*lt_t old_time;*/

	ts = litmus_clock();

	raw_spin_lock_irqsave(&slock, flags);
	#ifdef WANT_TIMERS_SCHED_EVENTS
	TRACE("-------------------- BUDGET EXHAUSTED at %llu node %d\n", litmus_clock(), n->id);
	#endif
	BUG_ON(!system_started);
	
	BUG_ON(!n->sched);
	
	/*old_time = ktime_to_ns(timer->_softexpires);
	#ifdef WANT_TIMERS_SCHED_EVENTS
	TRACE("INFO: OLD_TIME %llu\n", old_time);
	#endif*/

	//It may be affected by rounding problem
	now = n->sched->start_time + n->sched->budget;
	// 140118 - A timer may trigger simultaneously to a release event causing an invalid status of the tree
	
	if (lt_after(now /*- ROUND_ERROR*/, ts)) {
		#ifdef WANT_BASE_SCHED_EVENTS
		TRACE("WARNING: exhaustion overlaps release on node %d, now %llu ts %llu\n",
			n->sched->id, now, ts);
		#endif
	} else {
		TS_TREE_START;

		n->sched->budget = 0;
		n->armed = 0;

		// Budget exhaustion events can modify the state of the reduction tree.
		// It is necessary to trigger the scheduling operations of RUN.
		r_tree_traversal(n, now, &run.ts);
		run_resched_servers(now);

		TS_TREE_END;
		#ifdef WANT_TIMERS_SCHED_EVENTS
		TRACE("-------------------- BUDGET END\n");
		#endif
	}
	
	raw_spin_unlock_irqrestore(&slock, flags);

	
	return HRTIMER_NORESTART;
}

/***************************************************/

/*
 * Scheduling procedure called from inside a processor. Its decision is based
 * on the leaf server of the reduction tree assigned to the processor. Thus it
 * is indireclty related on the status of the reduction tree.
 */
static struct task_struct* run_schedule(struct task_struct * prev)
{
	struct task_struct* 	just_prev, *next;
	run_server_t*			just_server;
	cpu_entry_t*			entry;
	int 					out_of_time, sleep, preempt,
							np, exists, blocks, resched;
	
    raw_spin_lock(&slock);
        
    entry = local_entry;
    
    // If the system has not yet started (before time 0) we manage the situation 
    // accordingly (G-EDF).
    if (unlikely(!system_started 
    			#ifdef TERMINATION_CODE
    				|| exit_mode
    			#endif
    			)) {
	
		BUG_ON(entry->sched && entry->sched != prev);
		BUG_ON(entry->sched && !is_realtime(prev));
	
    	exists 	= entry->sched != NULL;
    	sleep 	= exists && is_completed(entry->sched);
    	blocks 	= exists && !is_running(entry->sched);
			
		if (!exists && __next_ready(&run.domain))
			resched = 1;		

		if (blocks)
			resched = 1;

		if (sleep && !blocks) {
			job_completion(entry->sched, !sleep);
			resched = 1;
		}

		next = NULL;
		
		if (resched || !exists) {
			
			if (entry->sched && !blocks)
				init_requeue(entry->sched);
			
			next = __take_ready(&run.domain);
			
		} else
			if (exists)
				next = prev;
			
			entry->sched = next;
			
    } else {
		// If the system is not in its initialization phase, schedule the tasks
		// as RUN commands.		
		
		// If no server is assigned to the processor, do not execute any
		// realtime task.
		if (!entry->s_sched) {			
			next = NULL;
		} else {
			// If a server is assigned to the processor, determines which task
			// to execute.
		
			// just_server is the server currently assigned to the processor to
			// be scheduled.					
			just_server = entry->s_sched;
			
			// just_prev identifies the task that is running inside the server.
			// If the server changed (becase of RUN scheduling decision),
			// just_prev != prev (from procedure's signature)			
			just_prev = just_server->highest_prio;
			
			// Define flags for the status of the last executing task
			exists      = just_prev != NULL;
			blocks      = exists && !is_running(just_prev);
	/*		out_of_time = exists && budget_enforced(just_prev) &&*/
	/*					budget_exhausted(just_prev);*/
			out_of_time = 0;
			np 	    	= exists && is_np(just_prev);
			sleep	    = exists && is_completed(just_prev);
			preempt     = preemption_needed(just_server);
			
			#ifdef WANT_ONLY_SCHED_EVENTS
			if (exists)
				TRACE_TASK(just_prev,
				   "blocks:%d out_of_time:%d np:%d sleep:%d preempt:%d "
				   "state:%d sig:%d\n",
				   blocks, out_of_time, np, sleep, preempt,
				   just_prev->state, 
				   signal_pending(just_prev));
			#endif

			// Understand through the flags if there is the necessity to reschedule
			// the work of the processor (change the running task).

			// If a preemption is needed, we must reschedule the server, since a
			// higher priority task is ready to execute.
			resched = preempt;
		
			// If the task is blocked, it is not performing any work, let switch it
			if (blocks)
				resched = 1;
			
			if (np && (out_of_time || preempt || sleep))
				request_exit_np(just_prev);

			if (!np && (out_of_time || sleep) && !blocks) {
				/*This call may call reschedule if task is tardy*/
				job_completion(just_prev, !sleep);
				resched = 1;
			}
			
			// Determines the next job to execute in the processor.
			next = NULL;
			
			// If the last scheduled task (or no task at all) from the server must
			// be switched with some other task, let's take this new task.
			// We must also remember to requeue the last task that the server was
			// executing.
			if (/*(!np || blocks) && */(resched || !exists)) {
				if (just_prev /*&& !blocks*/)
					// If the server was executing a task, we have to requeue it. This
					// call place the job in the right queue: the ready queue in the
					// server if the job has not completed, the release queue of the
					// run edf_domain if the job has completed. IMPORTANT: we have already
					// performed "job_completion", so we can call this procedure safely.
					requeue(just_prev);
				next = run_take_ready(just_server);
			} else
				// If the highest priority task in the server has not changed, let's
				// select it, which is the last scheduled task.	
				if (exists)
					next = just_prev;
		
			// We must also update the internal state of the server!
			just_server->highest_prio = next;
		}
	}
		
	if (next) {

		#ifdef WANT_ONLY_SCHED_EVENTS
		TRACE_TASK(next, "run: scheduled at %llu\n", litmus_clock());
		#endif

		tsk_rt(next)->completed = 0;

	} else {
		/*TRACE("run: becoming idle at %llu\n", litmus_clock());*/
	}
	// Notify the processor that the job to schedule has been determined.
    sched_state_task_picked();
	
	raw_spin_unlock(&slock);
	
	return next;
}

/*	Prepare a task for running in RT mode. Allow to catch the barrier release 
time. */
void run_release_at(struct task_struct *t, lt_t start)
{
	unsigned long 		flags;

	BUG_ON(!t);

	raw_spin_lock_irqsave(&slock, flags);
	
	if (global_time == INIT_HORIZON) {
		global_time = start - t->rt_param.task_params.phase
				   + t->rt_param.task_params.period;
		#ifdef WANT_BASE_SCHED_EVENTS
		TRACE("Global time initialized, system will start at %llu\n", global_time);
		#endif
	}

	release_at(t, start);

	trace_litmus_sys_release(&start);

	raw_spin_unlock_irqrestore(&slock, flags);
}

static void run_task_new(struct task_struct * t, int on_rq, int running)
{
	unsigned long 		flags;
	cpu_entry_t* 		entry;
	
	#ifdef WANT_BASE_SCHED_EVENTS
	TRACE_TASK(t,"task new, pid:%d\n",t->pid);
	#endif
	
	#ifdef TERMINATION_CODE
	tasks_added = tasks_added + 1;
	#endif
	
	raw_spin_lock_irqsave(&slock, flags);
	
	release_at(t, litmus_clock());	
	
	entry = local_entry;
	if (running) {
		entry->sched = t;
	} else {
		init_requeue(t);
	}
	
	raw_spin_unlock_irqrestore(&slock, flags);
}

static void run_task_wake_up(struct task_struct *task)
{
	unsigned long flags;
	lt_t now;
	
	#ifdef WANT_BASE_SCHED_EVENTS
	TRACE_TASK(task, "wake_up at %llu\n", litmus_clock());
	#endif
	
	raw_spin_lock_irqsave(&slock, flags);
	
	BUG_ON(is_queued(task));

	now = litmus_clock();

	if (is_tardy(task, now)) {

		#ifdef WANT_BASE_SCHED_EVENTS
		TRACE_TASK(task, "is_tardy, dl %llu, now %llu\n", get_deadline(task), now );
		#endif

		release_at(task, now);
		sched_trace_task_release(task);		
	}
	
	/* Only add to ready queue if it is not the currently-scheduled
	 * task. This could be the case if a task was woken up concurrently
	 * on a remote CPU before the executing CPU got around to actually
	 * de-scheduling the task, i.e., wake_up() raced with schedule()
	 * and won.
	 */

	init_requeue(task);
	
	#ifdef WANT_BASE_SCHED_EVENTS
	TRACE_TASK(task, "will release at %llu\n", get_release(task));
	#endif
	
	raw_spin_unlock_irqrestore(&slock, flags);
}

static void run_task_exit(struct task_struct * t)
{
	run_server_t*		s;
	unsigned long 		flags;
	// struct task_struct*	tmp_tsk;
	// lt_t				tmp_dl;

	//lt_t 				next_dl, now;
	//int 				i;

	
	#ifdef TERMINATION_CODE
	lt_t				wtf;	
	int ret;	
	cpu_entry_t*		entry;
	#endif
	
	raw_spin_lock_irqsave(&slock, flags);
	#ifdef TERMINATION_CODE
	if (exit_mode) { //Exit mode
		
		if (is_queued(t)) {
			remove(&run.domain, t);
		}
		entry = local_entry;
		if (entry->sched == t)
			entry->sched = NULL;
		
	} else { //RUN mode
	#endif
		s = get_s(t);	
	
		if (is_queued(t)) {
			bheap_delete(edf_ready_order, 
					 &s->ready_queue, 
					 tsk_rt(t)->heap_node);
		}
	
		if (s->highest_prio == t) {
			s->highest_prio = NULL;
			#ifdef WANT_BASE_SCHED_EVENTS
			TRACE_TASK(t, "EXIT: task scheduled\n");
			#endif
		
			// The task currently being scheduled by the server has exited. This may
			// cause problems for the tasks already queued (ready or release queue).
			// More specifically, we can have can have problems to execute these
			// already queued tasks since their servers can have no more replenishment
			// events (that are caused by the release of some job: if tasks terminate
			// their execution, they will not fire release events any more ). To avoid
			// this problem, we update the deadline and the budget of the server.

			// tmp_tsk = run_peek_ready(s);
			// if (tmp_tsk) {
			// 	tmp_dl = s->rn->e_dl;
			// 	s->rn->e_dl = get_earliest_deadline(s);
			// 	s->rn->budget += ((s->rn->e_dl - tmp_dl) * s->rn->rate[0]) 
			// 						/ s->rn->rate[1];
			// }
			
			/*140120 - This TERMINATION should not required sice "active" status on resched handle on exit */
			// now = s->rn->rel;
			// if (s->rn->e_dl == get_job_deadline(t, now)) {
			// 	i = 0;
			// 	while((i < s->task_n) && (s->task_ref[i] != t))
			// 		i += 1;
			// 	if (i < s->task_n)
			// 		s->task_ref[i] = NULL;
			// 	next_dl = get_earliest_deadline(s, now);
			// 	if(lt_after(next_dl, s->rn->e_dl)) {
			// 		s->rn->budget += ((next_dl - s->rn->e_dl) * s->rn->rate[0]) 
			// 						/ s->rn->rate[1];
			// 		s->rn->e_dl = next_dl;
			// 	}
			// }

			/* ------- Termination code ------- */
			#ifdef TERMINATION_CODE
			//Timer forces the experiment completion
			if (set_exit_timer) {
				wtf = litmus_clock() + (TERMINATION_TIME * ONE_MS);
				__hrtimer_start_range_ns(&exit_timer,
						 ns_to_ktime(wtf),
						 0,
						 HRTIMER_MODE_ABS_PINNED,
						 0);
				set_exit_timer = 0;

			}
		
			tasks_added = tasks_added - 1;
			if (tasks_added <= 0) {
				ret = hrtimer_try_to_cancel(&exit_timer);
			}
			#endif
			/* ------- Termination code ------- */

			wtf = litmus_clock();
			//trace_litmus_sys_release(wtf);

			// If the server of the exiting task is running, we must reschedule since
			// the exiting task has completed its job.
			if (s->running) {
				//140123
				run_resched_servers(wtf);
				//old - litmus_reschedule(s->cpu);
			}

		}
	#ifdef TERMINATION_CODE
	}
	#endif
	
	raw_spin_unlock_irqrestore(&slock, flags);

	#ifdef WANT_BASE_SCHED_EVENTS
	TRACE_TASK(t, "RIP\n");
	#endif
}

static long run_admit_task(struct task_struct *t)
{
        int s_id;
        run_server_t *s;
        
        // No task can be admitted if the system is already running.
        if (system_started)
        {	
        	#ifdef WANT_BASE_SCHED_EVENTS
        	TRACE_TASK(t,"run: rejected\n");
        	#endif
        	return -EINVAL;
        }
        
        s_id = get_s_id(t);
        // Check whether the server that should accomodate the task is valid.
        if ((s_id < 0) || (s_id >= run.n_servers))
        {
        	#ifdef WANT_BASE_SCHED_EVENTS
        	TRACE_TASK(t,"rejected, server unknown\n");
        	#endif
        	return -EINVAL;
        }
        
        // Error if the server is not initialized.
        if (run.servers[s_id] == NULL) {
        
        	#ifdef WANT_BASE_SCHED_EVENTS
        	TRACE_TASK(t,"rejected, server NULL\n");
        	#endif
        	return -EINVAL;
        }
        
        s = run.servers[s_id];
        t->rt_param.server = s;

        /* -- LIMA */
        s->task_ref[s->task_n] = t;
        s->task_n += 1;
        /* -- LIMA */

        #ifdef WANT_BASE_SCHED_EVENTS
        TRACE_TASK(t,"admitted from server %d\n", s_id);
        #endif
        return 0;
}

/* Deallocate RUN data structures.
 */
static void cleanup_run(void)
{
	int s_id;
	
	if (!r_tree_empty(&run.rtree)) {
		t_st_init(&run.ts);
		r_tree_dealloc(run.rtree.root, &run.ts);
		INIT_R_TREE(&run.rtree);
		TRACE("RUN: Reduction tree correctly deallocate!\n");
	}

	for(s_id = 0; s_id < MAX_SERVER; s_id++) {	
		if (run.servers[s_id]) {	
			kfree(run.servers[s_id]);
			run.servers[s_id] = NULL;
			TRACE("RUN: Server %d correctly deallocated\n", s_id);	
		}
	}
	
}

/* ------- Termination code ------- */
#ifdef TERMINATION_CODE
static enum hrtimer_restart on_exit_experiments(struct hrtimer *timer) {
	int s_id, cpu;
	run_server_t *s;
	cpu_entry_t *entry;
	unsigned long flags;
	struct task_struct *t;
	
	raw_spin_lock_irqsave(&slock, flags);
	
	if (tasks_added > 0) {	
		//sposta task in coda ready
		for(s_id = 0; s_id < run.n_servers; s_id++) {
			s = run.servers[s_id];
			if (s) {
				if (s->highest_prio) {
					add_ready(&run.domain, s->highest_prio);
					s->highest_prio = NULL;
				}
				while(!bheap_empty(&s->ready_queue)) {	
					t = bheap2task(bheap_take(edf_ready_order, &s->ready_queue));
					add_ready(&run.domain, t);
				}
			}
		}	
		tasks_added = 0;
		exit_mode = 1;
		
		for(cpu = 0; cpu < run.n_cpus; cpu ++) {	
			entry = remote_entry(cpu);
			if (entry)
				entry->sched = NULL;	
		}
		litmus_reschedule_local();
	}
	
	raw_spin_unlock_irqrestore(&slock, flags);
	
	return HRTIMER_NORESTART;	
}
#endif
/* ------- Termination code ------- */

static long run_deactivate_plugin(void)
{
	cleanup_run();
	return 0;
}

static long run_activate_plugin(void)
{
	int cpu;
	cpu_entry_t *entry;
	
	raw_spin_lock_init(&slock);
	
	cleanup_run();
	
	run.n_cpus = num_online_cpus();
	run.n_servers = 0; // it will be initialized during the tree construction
	
	/* ------- Termination code ------- */
	#ifdef TERMINATION_CODE
	tasks_added = 0;
	set_exit_timer = 1;
	exit_mode = 0;
	hrtimer_init(&exit_timer, CLOCK_MONOTONIC, HRTIMER_MODE_ABS);
	exit_timer.function = on_exit_experiments;
	#endif
	/* ------- Termination code ------- */

	t_st_init(&run.ts);
	
	for(cpu = 0; cpu < run.n_cpus; cpu++) {
		TRACE("run: initializing CPU #%d.\n", cpu);
		entry = remote_entry(cpu);
		entry->cpu = cpu;
		entry->s_sched = NULL;
		entry->sched = NULL;
		entry->resched = 0;
	}
	
	global_time = INIT_HORIZON;
	edf_domain_init(&run.domain, NULL, run_release_jobs);
	
	INIT_R_TREE(&run.rtree);
	curr_node = NULL;
	t_st_init(&run.ts);	
	
	//TRACE("System initialized, %d processors\n", run.n_cpus);
	return 0;
}

/*	Plugin object	*/
static struct sched_plugin run_plugin __cacheline_aligned_in_smp = {
	.plugin_name		= "RUN",
	.task_new			= run_task_new,
	.complete_job		= complete_job,
	.release_at			= run_release_at,
	.task_exit			= run_task_exit,
	.schedule			= run_schedule,
	.task_wake_up		= run_task_wake_up,
	.admit_task			= run_admit_task,
	.activate_plugin	= run_activate_plugin,
	.deactivate_plugin	= run_deactivate_plugin,
};


static int __init init_run(void)
{	
	return register_sched_plugin(&run_plugin);
}

static void clean_run(void)
{
	cleanup_run();
}

module_init(init_run);
module_exit(clean_run);
